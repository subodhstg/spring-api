package com.training.spring.service;

import com.training.spring.exception.EmployeeException;
import com.training.spring.model.Employee;
import com.training.spring.repository.EmployeeRepository;
import com.training.spring.request.EmployeeReq;
import com.training.spring.response.EmployeeResponse;
import com.training.spring.util.EmployeePopulator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Slf4j

public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Transactional
    public EmployeeResponse createEmployeeResponse(EmployeeReq employeeReq){
        validateMandatoryFields(employeeReq);
        Employee employee = employeeRepository.save(EmployeePopulator.populateEmployeeFromReq(employeeReq));
        return EmployeePopulator.populateEmployeeResponse(employee);
    }

    @Transactional(readOnly = true)
    public Employee getEmployee(String empId) {
        Employee  emp = employeeRepository.findById(empId).get();
        log.info("Employee "+emp.toString());
        return emp;
    }

    public void validateMandatoryFields(EmployeeReq employeeReq){
        log.info(employeeReq.getEmpId());
        if(StringUtils.isBlank(employeeReq.getEmpId())) {
            throw new EmployeeException("ER001","Employee ID cannot be left blank");
        }
        if(StringUtils.isBlank(employeeReq.getName())) {
            throw new EmployeeException("ER002","Employee Name cannot be left blank");
        }
    }
}
