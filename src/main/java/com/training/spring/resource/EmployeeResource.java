package com.training.spring.resource;

import com.training.spring.exception.EmployeeException;
import com.training.spring.request.EmployeeReq;
import com.training.spring.response.EmployeeResponse;
import com.training.spring.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("employee")
public class EmployeeResource {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    public EmployeeResponse addEmployee(@RequestBody EmployeeReq employeeReq){

        try {

            return employeeService.createEmployeeResponse(employeeReq);

        }catch(EmployeeException ex){
            return ex.getEmployeeResponse(employeeReq);
        }catch(Exception ex){
            EmployeeException employeeException = new EmployeeException("S001",ex.getMessage());
            return employeeException.getEmployeeResponse(employeeReq);

        }
    }


}
