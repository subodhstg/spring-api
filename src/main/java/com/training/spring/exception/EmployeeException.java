package com.training.spring.exception;

import com.training.spring.request.EmployeeReq;
import com.training.spring.response.EmployeeResponse;
import com.training.spring.util.EmpErrors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class EmployeeException extends RuntimeException {
    private final String errorCode;

    public EmployeeException(String errorCode, String errorMsg){
        super(errorMsg);
        this.errorCode=errorCode;
    }

    public EmployeeResponse getEmployeeResponse(EmployeeReq employeeReq){
        EmployeeResponse employeeResponse= new EmployeeResponse();
        BeanUtils.copyProperties(employeeReq,employeeResponse);
        EmpErrors empErrors = new EmpErrors();
        empErrors.setErrorCode(errorCode);
        empErrors.setErrorMsg(getMessage());
        employeeResponse.setErrors(empErrors);
        return employeeResponse;
    }
}


