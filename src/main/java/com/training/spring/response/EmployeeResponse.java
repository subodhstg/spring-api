package com.training.spring.response;

import com.training.spring.util.EmpErrors;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EmployeeResponse implements Serializable {
    private String empId;
    private String name;
    private String city;
    private Double salary;
    private EmpErrors errors;
}
