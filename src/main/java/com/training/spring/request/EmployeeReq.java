package com.training.spring.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EmployeeReq implements Serializable {
    private String empId;
    private String name;
    private String city;
    private Double salary;
}
