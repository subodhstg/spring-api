package com.training.spring.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@ToString
@Table(name="employee")
public class Employee implements Serializable {
    @Id
    @Column(name = "empId", nullable = false)
    private String empId;

    @Column(name = "name", nullable =true)
    private String name;

    @Column(name = "city")
    private String city;

    @Column(name = "salary")
    private Double salary;
}
