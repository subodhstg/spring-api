package com.training.spring.util;

import com.training.spring.model.Employee;
import com.training.spring.request.EmployeeReq;
import com.training.spring.response.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeePopulator {
    private EmployeePopulator(){}
    public static EmployeeResponse populateEmployeeResponse(Employee employee){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }

    public static Employee populateEmployeeFromReq(EmployeeReq employeeReq){
        Employee employee=new Employee();
        BeanUtils.copyProperties(employeeReq,employee);
        return employee;
    }
}
