package com.training.spring.util;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EmpErrors implements Serializable {
    private String errorCode;
    private String errorMsg;
}
