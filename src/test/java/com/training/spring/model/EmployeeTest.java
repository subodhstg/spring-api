package com.training.spring.model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class EmployeeTest {

    private Employee employee;

    @Before
    public void init() {
        employee = new Employee();
    }

    @Test
    public void testEmp(){
        employee.setEmpId("a001");
        employee.setName("subodh");
        employee.setCity("India");
        employee.setSalary(1110.33);
        Assert.assertNotNull(employee);
    }
}